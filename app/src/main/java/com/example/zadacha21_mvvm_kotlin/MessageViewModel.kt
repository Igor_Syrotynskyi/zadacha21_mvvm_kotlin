package com.example.zadacha21_mvvm_kotlin

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * View Model class
 * implementation all functions from repository
 */
class MessageViewModel(application: Application) : AndroidViewModel(application) {
    private val messageRepository: MessageRepository = MessageRepository(application)

    /**
     * This method requests to Repository that to get messages from server, convert it to entity and insert to database
     */
    fun updateMessagesFromServer() {
        viewModelScope.launch(Dispatchers.IO) {
            messageRepository.getMessagesFromServerAndInsertToDB()
        }

    }

    /**
     * This variable gets all messages from database
     *
     * @return list of messages
     */
    val allMessages: LiveData<List<MessageEntity>>
        get() = messageRepository.allMessages

    /**
     * This method finds messages in database by entering userID
     *
     * @param sortingUserId entering userID for sorting
     * @return list of messages with searching userID
     */
    fun findMessageByUserId(sortingUserId: String): LiveData<List<MessageEntity>> {
        return messageRepository.findMessagesByUserId(sortingUserId)
    }

}
