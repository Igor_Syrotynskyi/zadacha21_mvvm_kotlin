package com.example.zadacha21_mvvm_kotlin

import retrofit2.Call
import retrofit2.http.GET

/**
 * This interface implements requests to the server
 */
interface JsonPlaceHolderApi {
    //get messages from server
    // 'posts', because the information we need lies on the server, with local addressing 'posts'
    @get:GET("posts")
    val messages: Call<List<MessageApiModel>>
}
