package com.example.zadacha21_mvvm_kotlin

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var messageViewModel: MessageViewModel
    private lateinit var messageAdapter: MessageAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initializationAdapter()
        viewModelObservers()
        addClickListenerToButtons()
    }

    /**
     * This method initializes recyclerview and adapter
     */
    private fun initializationAdapter() {
        messageAdapter = MessageAdapter()
        val recyclerView = findViewById<RecyclerView>(R.id.messagesRecyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.setHasFixedSize(true)
        recyclerView.adapter = messageAdapter
    }

    /**
     * This method initializes ViewModel variable and created Live Data observer to message database
     */
    private fun viewModelObservers() {
        messageViewModel = ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory.getInstance(this.application)).get(MessageViewModel::class.java)
        messageViewModel.allMessages.observe(this, { messages -> //update adapter
            messageAdapter.setMessages(messages)
        })
    }

    /**
     * This method added click listeners to all buttons
     */
    private fun addClickListenerToButtons() {
        updateButton.setOnClickListener(this)
        sortByUserIdButton.setOnClickListener(this)
    }

    /**
     *
     * This method handles the push of a buttons
     *
     * @param v - id of buttons
     */
    override fun onClick(v: View) {
        when (v.id) {
            R.id.updateButton -> messageViewModel.updateMessagesFromServer()
            R.id.sortByUserIdButton -> {
                val intent = Intent(this, SortByUserId::class.java)
                startActivityForResult(intent, SORT_REQUEST)
            }
        }
    }

    /**
     * This method gets results from SortByUserId activity
     *
     * @param requestCode - request code
     * @param resultCode  - result code
     * @param data        - activity
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        // set value of requestCode and resultCode
        super.onActivityResult(requestCode, resultCode, data)
        // if requestCode and resultCode is OK
        if (resultCode == RESULT_OK && requestCode == SORT_REQUEST) {
            //get result from search user ID layout
            val sortingUserId = data?.getStringExtra(SortByUserId.EXTRA_USER_ID)
            //get result from DB
            messageViewModel.findMessageByUserId(sortingUserId!!).observe(this, { sortedMessageEntities -> messageAdapter.setMessages(sortedMessageEntities) })
        } else {
            // if requestCode and resultCode is not OK
            Toast.makeText(this, "Messages was not sorted", Toast.LENGTH_SHORT).show()
        }
    }

    companion object {
        const val SORT_REQUEST = 1
    }
}
