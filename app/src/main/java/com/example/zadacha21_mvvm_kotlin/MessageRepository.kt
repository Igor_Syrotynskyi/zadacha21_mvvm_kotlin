package com.example.zadacha21_mvvm_kotlin

import android.app.Application
import android.os.AsyncTask
import android.util.Log
import androidx.lifecycle.LiveData
import com.example.zadacha21_mvvm_kotlin.MessageDatabase.Companion.getInstance
import retrofit2.*
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*

/**
 * Repository class
 * get data from server and insert there to local database
 * implementation all functions which help us work with messages from database
 */
class MessageRepository(application: Application) {
    private val messageDao: MessageDao
    private lateinit var jsonPlaceHolderApi: JsonPlaceHolderApi
    private val TAG = "RetrofitMessage:"

    /**
     * This method initialize the Retrofit variable
     *
     * @return
     */
    private fun createRetrofit(): JsonPlaceHolderApi {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://jsonplaceholder.typicode.com/") //Specify the resource where exactly the data lies.
            .addConverterFactory(GsonConverterFactory.create()) //Specify that you want to extract in GSON format
            .build()
        return retrofit.create(JsonPlaceHolderApi::class.java).also { jsonPlaceHolderApi = it }
    }//Show text of error in log//callback is not successful//request is not successful

    /**
     * This method gets messages from server and save it in messageApiModel variable.
     * Then call method which convert from ApiModel list to Entity list.
     * Then call method which insert messages to local database
     */
    suspend fun getMessagesFromServerAndInsertToDB() {
        val messageApiModelList = messagesFromServer()
        //convert messages from server and set it to database
        val messageEntityList =
            convertingMessagesFromApiModelToEntity(messageApiModelList)
        //insert messages to local database
        messageDao.insertMessagesCameFromServer(messageEntityList)
    }

    private suspend fun messagesFromServer(): List<MessageApiModel> {
        //sending request to server
        val call = jsonPlaceHolderApi.messages.awaitResponse()
        return call.body()!!

    }

    /**
     * This method got list of messages from server, convert it to MessageEntity list
     *
     * @param messageApiModelList -the list of messages from server
     * @return - converted to MessageEntity list
     */
    private fun convertingMessagesFromApiModelToEntity(messageApiModelList: List<MessageApiModel>): List<MessageEntity> {
        val messageEntityList: MutableList<MessageEntity> = ArrayList()
        for (messageApiModel in messageApiModelList) {
            val messageEntity = MessageEntity(
                userId = messageApiModel.userId,
                id = messageApiModel.id,
                title = messageApiModel.title,
                text = messageApiModel.text
            )
            messageEntityList.add(messageEntity)
        }
        return messageEntityList
    }

    /**
     * This method get Live Data list with all messages in database
     *
     * @return the List with all messages
     */
    val allMessages: LiveData<List<MessageEntity>>
        get() = messageDao.allMessages

    /**
     * This method finds all messages in database by userID
     *
     * @param sortingUserId - number of userID which we search
     * @return list of messages with searching ID
     */
    fun findMessagesByUserId(sortingUserId: String): LiveData<List<MessageEntity>> {
        return messageDao.findMessagesByUserId(sortingUserId)
    }

    init {
        createRetrofit()
        val messageDatabase = getInstance(application)
        messageDao = messageDatabase.messageDao()
    }
}
