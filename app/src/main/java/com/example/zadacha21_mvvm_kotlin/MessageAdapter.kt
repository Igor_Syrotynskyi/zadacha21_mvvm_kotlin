package com.example.zadacha21_mvvm_kotlin

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.zadacha21_mvvm_kotlin.MessageAdapter.MessageHolder
import java.util.*

/**
 * Adapter for RecyclerView class
 */
class MessageAdapter : RecyclerView.Adapter<MessageHolder>() {
    private var messages: List<MessageEntity> = ArrayList()

    /**
     * This method get list of messages which should to show in Recycler View
     * @param messages - list of contacts
     */
    fun setMessages(messages: List<MessageEntity>) {
        this.messages = messages
        notifyDataSetChanged()
    }

    /**
     * This method find layout with how should to view holder
     * @param parent
     * @param viewType
     * @return - holder
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.message_item, parent, false)
        return MessageHolder(itemView)
    }

    /**
     * This method calls every time when user scroll the list of messages and refresh information on screen(set text in views from item layout)
     *
     * @param holder   - View Holder
     * @param position - where will be insert View Holder
     */
    override fun onBindViewHolder(holder: MessageHolder, position: Int) {
        val (userId, id, title, text) = messages[position]
        holder.idTextView.text = "ID: $id"
        holder.userIdTextView.text = "User ID: $userId"
        holder.titleTextView.text = "Title: $title"
        holder.textTextView.text = "Text: $text"
    }

    /**
     * This method gets sum of items
     *
     * @return - amount of items
     */
    override fun getItemCount(): Int {
        return messages.size
    }

    /**
     * This class implements how to should look the View Holder
     */
    class MessageHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val idTextView: TextView = itemView.findViewById(R.id.idTextView)
        val userIdTextView: TextView = itemView.findViewById(R.id.userIdTextView)
        val titleTextView: TextView = itemView.findViewById(R.id.titleTextView)
        val textTextView: TextView = itemView.findViewById(R.id.textTextView)

    }
}
