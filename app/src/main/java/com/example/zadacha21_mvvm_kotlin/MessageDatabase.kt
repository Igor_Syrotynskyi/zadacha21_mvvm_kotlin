package com.example.zadacha21_mvvm_kotlin

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

/**
 * This is Message database class
 */
@Database(entities = [MessageEntity::class], version = 1)
abstract class MessageDatabase : RoomDatabase() {
    abstract fun messageDao(): MessageDao

    companion object {
        private var instance: MessageDatabase? = null

        /**
         * This method creates database ONLY ones and return instanse of it.. If database is already created, this method only return instance on it.
         *
         * @param context
         * @return instance of database.
         */
        @JvmStatic
        @Synchronized
        fun getInstance(context: Context): MessageDatabase {
            if (instance == null) {
                instance = Room.databaseBuilder(context.applicationContext,
                        MessageDatabase::class.java, "contact_databese")
                        .fallbackToDestructiveMigration()
                        .build()
            }
            return instance!!
        }
    }
}
