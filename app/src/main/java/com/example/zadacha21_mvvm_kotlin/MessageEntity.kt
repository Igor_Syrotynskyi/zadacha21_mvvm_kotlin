package com.example.zadacha21_mvvm_kotlin

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * This class describes a database entity.
 */
@Entity(tableName = "message_table")
data class MessageEntity (
    val userId: Int,
    val id: Int,
    val title: String,
    val text: String,
    @PrimaryKey(autoGenerate = true)
    var position:Int = 0,
)
