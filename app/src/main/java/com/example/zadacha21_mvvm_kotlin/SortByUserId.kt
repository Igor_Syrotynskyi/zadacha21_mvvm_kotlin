package com.example.zadacha21_mvvm_kotlin

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.sort_by_user_id.*

class SortByUserId : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.sort_by_user_id)
        configureLayoutElements()

    }

    override fun onClick(v: View) {
        val intent = Intent()
        //press search by User ID Button
        if (v.id == R.id.chooseUserIdButton) {
            // get user ID
            val sortingUserId = chooseUserId()
            intent.putExtra(EXTRA_USER_ID, sortingUserId)
            //sent data to main activity
            setResult(RESULT_OK, intent)
            finish()
        }
    }

    /**
     * This method user ID for sorting
     * @return userID
     */
    private fun chooseUserId(): String {

        val intChooseUserId = numberPickerUserId.value
        return intChooseUserId.toString()
    }

    /**
     * This method configured layout elements
     */
    private fun configureLayoutElements() {
        numberPickerUserId.minValue = 1
        numberPickerUserId.maxValue = 100
        chooseUserIdButton.setOnClickListener(this)
    }

    companion object {
        const val EXTRA_USER_ID = "package com.example.zadacha21_mvvm_kotlin.EXTRA_USER_ID" //key variable for passing userId to  MainActivity
    }
}
