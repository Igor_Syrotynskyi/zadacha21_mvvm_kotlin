package com.example.zadacha21_mvvm_kotlin

import com.google.gson.annotations.SerializedName

/**
 * This class describes messages from the server
 */
data class MessageApiModel(
        val userId: Int,
        val id: Int,
        val title: String,
        @SerializedName("body") // Because the key in JSON is called "body" and we push it into "text"
        val text: String
)
