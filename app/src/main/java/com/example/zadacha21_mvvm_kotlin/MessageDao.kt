package com.example.zadacha21_mvvm_kotlin

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface MessageDao {
    //insert message to database
    @Insert
    suspend fun insertMessagesCameFromServer(messageEntityToDatabaseFromServer: List<MessageEntity>)

    //get all messages from database
    @get:Query("SELECT * FROM message_table")
    val allMessages: LiveData<List<MessageEntity>>

    //get messages from database by sorting userID
    @Query("SELECT * FROM message_table WHERE userID LIKE :sortingUserId")
    fun findMessagesByUserId(sortingUserId: String): LiveData<List<MessageEntity>>
}
